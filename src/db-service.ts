import { Role } from './entity/Role'

async function setupDB() {
  const userRole = await Role.findOne({ role: 'user' })
  const godRole = await Role.findOne({ role: 'god' })

  if (!userRole) {
    let user = new Role()
    user.role = 'user'
    user.description = 'Basic User Role'
    user.level = 0
    user.save()
  }

  if (!godRole) {
    let god = new Role()
    god.role = 'god'
    god.description = 'The Superest of Admins'
    god.level = 9001
    god.save()
  }
}

export { setupDB }
