import { gql } from 'apollo-server-express'
import userType from './user-type'
import roleType from './role-type'

const baseType = gql`
  type Query { base: String }
  type Mutation { base: String }
` // A base Query and Mutation are required to modularize types

const typeDefs = [ baseType, userType, roleType ] // Any other types get added to this list

export default typeDefs