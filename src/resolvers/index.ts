import { merge } from 'lodash'
import { userResolver } from './user-resolver'

const resolvers = merge(userResolver) // Any other resolvers get added to this _.merge()

export default resolvers